package org.rgsoft.twitchtracker.controller;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class MainController {
	private static final Logger log = Logger.getLogger(MainController.class.getName());

	@RequestMapping(value = { "/" })
	@ResponseBody
	public String test() {
		return "Hello World!";
	}
}

