# Show case for: IBM Kubernetes Service pipe

This repository showcases how to use the IBM Kubernetes Service pipe in a Bitbucket CI/CD pipeline.
You can find details about the pipe in it's repository here: https://bitbucket.org/rgruber/ibm-k8s-pipe/

## deployment.yaml
This is a pretty simple deployment descriptor. It spawns a single pod using the image which we will build using the pipeline showcased in this repository. 

Please note the following important parts of the deployment:

```yaml
spec:
     containers:
     - name: helloworld
       image: docker.io/rogruber/helloworld:production
```
We specify a particular image tag to be used for our container. The pipeline will always tag the newly created image with this tag, effectively updating the tag to the latest image.

```yaml
       readinessProbe:
         httpGet:
           path: /
           port: 8080
         initialDelaySeconds: 5
         periodSeconds: 2
```
By specifying a readinessProbe we ensure that Kubernetes will know if this pod is ready or not. As long as the new pod is not ready, the old one will not be terminated. This should make sure that we do not have a downtime when updating to a newer release.

```yaml
 strategy:
   rollingUpdate:
     maxSurge: 1
     maxUnavailable: 0
   type: RollingUpdate
```
We want to make sure that Kubernetes performs a rolling update once we issue a rollout restart. maxUnavailable:0 makes sure that our only pod does not get stopped until the new pod is available and ready.



## bitbucket-pipelines.yml

This repo's pipeline uses three steps:

* Build
* build docker
* IBM rollout

## Build
The first step is pretty straight forward. It uses maven to compile and package the helloworld app. It stores the generated JAR file as artifact so that it can be used in later steps.

## build docker
This step uses the JAR from the previous step to build a docker image and publish it to the Docker Hub repository. 
Note: The created image is tagged with the "production" tag. This is important as this is the tag used in the deployment.yaml as image reference for the containers. When we trigger a rollout restart in the next step, Kubernetes will fetch the latest version of the production image from the docker repository and use it to start new pods.

## IBM rollout
This step finally references the ibm-k8s-pipe custom pipe. It uses various variables to reference the IBM cloud user, Kubernetes cluster and deployment which should be restarted.