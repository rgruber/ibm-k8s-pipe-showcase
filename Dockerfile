FROM openjdk:11-oracle
VOLUME /tmp

ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

ENV GOOGLE_APPLICATION_CREDENTIALS=/opt/gcp.json
ENTRYPOINT ["java","-cp","app:app/lib/*","org.rgsoft.twitchtracker.SpringBootExampleApplication"]
